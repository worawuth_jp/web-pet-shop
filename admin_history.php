<?php
require 'function.inc.php';
include 'chk_sess.php';
if (isset($_GET["delete"])) {
    $iddelete = $_GET["delete"];
    $sql = "DELETE FROM bill_product WHERE id_bill='$iddelete'";
    $result3 = mysqli_query($con, $sql);


    if ($result3) {
        echo "<center><h3>ลบเรียบร้อยแล้ว</h3>";
        echo "<meta http-equiv='refresh' content='0;url=admin_history.php' />";

    } else {
        echo "<h3>ERROR : ไม่สามารถแก้ไข</h3>";
        echo "<META HTTP-EQUIV=\"REFRESH\" CONTENT=\"1; URL=admin_history.php?status=failue\">";
    }
}
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title> <?php echo $sys_title; ?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body topmargin="0">

<?php require_once('head.php'); ?></td>


<form id="form1" name="form1" method="post" action="admin_history.php">
     <h3 align="center">ค้นหา

     <select name='startdate'>
	 <option value="">--เลือกเดือน--</option>
    <?php
$query2 = "SELECT `detail_bill`.`id_detail`,`detail_bill`.`id_bill`,`detail_bill`.`id_product`,
SUM(`detail_bill`.`qty_product`) as num,SUM(`detail_bill`.`amounts`)
as 'Amounts',`detail_bill`.`date_detail` ,Date_format(`bill_product`.`date_bill`,'%m/%Y')
as 'DATE/MONTH' FROM detail_bill INNER JOIN `bill_product` ON `bill_product`.`id_bill` = `detail_bill`.`id_bill`
GROUP BY MONTH(`bill_product`.`date_bill`)";
 $result34 = mysqli_query($con,$query2);
while($row14 = $result34->fetch_array())
{?>

<option value='<?php echo $row14['DATE/MONTH']; ?>'><?php echo $row14['DATE/MONTH'];?></option>
<?php } ?>
  </select>

     <select name='enddate'>
	 <option value="">--เลือกเดือน--</option>
    <?php
$query2 = "SELECT `detail_bill`.`id_detail`,`detail_bill`.`id_bill`,`detail_bill`.`id_product`,
SUM(`detail_bill`.`qty_product`) as num,SUM(`detail_bill`.`amounts`)
as 'Amounts',`detail_bill`.`date_detail` ,Date_format(`bill_product`.`date_bill`,'%m/%Y')
as 'DATE/MONTH' FROM detail_bill INNER JOIN `bill_product` ON `bill_product`.`id_bill` = `detail_bill`.`id_bill`
GROUP BY MONTH(`bill_product`.`date_bill`)";
 $result34 = mysqli_query($con,$query2);
while($row14 = $result34->fetch_array())
{?>
<option value='<?php echo $row14['DATE/MONTH']; ?>'><?php echo $row14['DATE/MONTH'];?></option>
<?php } ?>
  </select>

       <input name="submit" type="submit" value="ค้นหา" />
     </h3>
     <p align="center">&nbsp;</p>
   </form>
<div style="padding: 20px;"></div>
<div class="container">
    <div class="row">
    <?php


	$startdate = "";
	$enddate = "";
  $start = "";
  $end = "";

  if(isset($_POST["startdate"]) && isset($_POST["enddate"])){
    $start = $_POST["startdate"];
    $startdate = '01/'.$start;
    $end = $_POST["enddate"];
    $enddate = '01/'.$end;
  }

	//echo$startdate;
 $sql_income = "SELECT `detail_bill`.`id_detail`,`detail_bill`.`id_bill`,`detail_bill`.`id_product`,
SUM(`detail_bill`.`qty_product`) as num,SUM(`detail_bill`.`amounts`)
as 'Amounts',`detail_bill`.`date_detail` ,Date_format(`bill_product`.`date_bill`,'%m/%Y')
as 'DATE/MONTH' FROM detail_bill INNER JOIN `bill_product` ON `bill_product`.`id_bill` = `detail_bill`.`id_bill`
GROUP BY MONTH(`bill_product`.`date_bill`)";
///print_r($sql_income);



if ($start != '' and $end != '') {
$sql_income = "SELECT `detail_bill`.`id_detail`,`detail_bill`.`id_bill`,`detail_bill`.`id_product`,SUM(`detail_bill`.`qty_product`) as num,SUM(`detail_bill`.`amounts`) as 'Amounts',`detail_bill`.`date_detail` ,Date_format(`bill_product`.`date_bill`,'%m/%Y') as 'DATE/MONTH' FROM detail_bill
INNER JOIN `bill_product` ON `bill_product`.`id_bill` = `detail_bill`.`id_bill`
WHERE `bill_product`.`date_bill` BETWEEN str_to_date('{$startdate}','%d/%m/%Y') AND DATE_ADD(str_to_date('{$enddate}','%d/%m/%Y'),INTERVAL 1 MONTH)
GROUP BY MONTH(`bill_product`.`date_bill`)";
//print_r($sql_income);
}


 $result_income = mysqli_query($con,$sql_income);






?>


        <div class="col-sm-3"> <?php require_once('menuleft.php'); ?>  </div>
        <div class="col-sm-9">
            <center><h1>รายได้</h1>วันที่<?php echo $start; ?>-<?php echo $end; ?>
              <table id="income" class="table text-center border-top">
                <thead>
                  <tr>
                    <th scope="col" class="text-center "><label class="font-weight-bold">เดือน/ปี</label></th>
                    <th scope="col" class="text-center"><label class="font-weight-bold">จำนวนสินค้าที่ขาย/เดือน</label></th>
                    <th scope="col" class="text-right"><label class="font-weight-bold">ยอดขายรายเดือน</label></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <?php
            $total = 0;
            while ($record = $result_income->fetch_array()){
                $total += $record['Amounts'];

        ?>
                    <td ><?php echo $record['DATE/MONTH'];?></td>
                    <td ><?php echo $record['num'];?></td>
                    <td class="text-right"><label><?php echo number_format($record['Amounts'], 2); ?></label></td>
                  </tr>
                  <?php
        }
        ?>
                  <tr class="border-bottom">
                    <td colspan="2" class="text-right border-right font-weight-bold">รวม</td>
                    <td class="text-right font-weight-bold"><?php echo number_format($total,2); ?></td>
                  </tr>
                </tbody>
              </table>
              <p>&nbsp;</p>
            </center>

        </div>
    </div>
</div>

<?php require_once('down.php'); ?>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
</script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
</body>
</html>
