<?php
require 'function.inc.php';
if (isset($_SESSION['sess_id'])!="") {
if ( !isset($_SESSION["total"]) ) {
  // สร้าง SESSION เก็บผลรวมเงิน
   $_SESSION["total"] = 0;
   for ($i=0; $i< count($products); $i++) {
    $_SESSION["qty"][$i] = 0;
   $_SESSION["amounts"][$i] = 0;

  }
 }
 if ( isset($_GET["add"]) )
   {
    // GET เพื่อเก็บข้อมูลเพิ่มสินค้า
   $i = $_GET["add"];


   $qty = $_SESSION["qty"][$i] + 1;  // บวกสินค้าเพิ่มจากเดิม
   $_SESSION["amounts"][$i] = $amounts[$i] * $qty;// เอาราคาสินค้าคูณจำนวน
   $_SESSION["cart"][$i] = $i;// ลำดับสินค้า
   $_SESSION["qty"][$i] = $qty;// จำนวนสินค้า
 }

  if ( isset($_GET["delete"]) )
   {
    // ลบสินค้า
   $i = $_GET["delete"];
   $qty = $_SESSION["qty"][$i];
   $qty = 0;
   $_SESSION["qty"][$i] = $qty;    // ลบสินค้าตามจำนวน
   if ($qty == 0) {
    $_SESSION["amounts"][$i] = 0;
    unset($_SESSION["cart"][$i]);// ลบสินค้าตามจำนวนแต่ละ$_SESSION
  }
 else
  {
   $_SESSION["amounts"][$i] = $amounts[$i] * $qty;// รวมผลสินค้า
  }
 }
}else{
    msgbox('กรุณาเข้าสู่ระบบ','index.php');
}
 if ( isset($_GET["update"]) )
   {
    $amount_array = $_POST['ok'];
    foreach($amount_array as $id=>$amount)
    {
      $_SESSION["qty"][$id] = $amount;
      $qty = $_SESSION["qty"][$id];
      $_SESSION["amounts"][$id] = $amounts[$id] * $qty;
      $_SESSION["cart"][$id] = $id;// ลำดับสินค้า

      if ($qty == 0) { //หากสินค้าน้อยกว่า0
        $_SESSION["amounts"][$id] = 0;
        unset($_SESSION["cart"][$id]);// ลบสินค้าตามจำนวนแต่ละ$_SESSION
      }
    }
}
 ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $sys_title ;?></title>
</head>
<body topmargin="0">
<?php require_once('head.php'); ?></td>
<div style="padding: 20px;"></div>
<div class="container">
  <div class="row">
    <div class="col-sm-3"> <?php require_once('menuleft.php'); ?>  </div>
    <div class="col-sm-9"><center>      <h1>เลือกบริการ</h1></center>
	<form id="frmcart" name="frmcart" method="post" action="?update=true">
  <?php
 if ( isset($_SESSION["cart"]) ) {
 ?>
            <table class="table table-hover" border="1" width="600">
                <thead>
                    <tr >
                        <th width="300">ชื่อสินค้า</th>
                        <th width="50">จำนวน</th>
                        <th width="50" class="text-center">ราคา</th>
                        <th width="100" class="text-center">รวม</th>
                        <th width="100"> </th>
                    </tr>
                </thead>
                <tbody>
 <?php
 $total = 0;

 foreach ( $_SESSION["cart"] as $i ) {
  // วนลูปสินค้าในเซคชั่น
 ?>
                    <tr>
                        <td >
              <?php echo( $products[$_SESSION["cart"][$i]] ); ?></td>
                        <td style="text-align: center">
              <input type="number" name='ok[<?php echo( $_SESSION["cart"][$i] ); ?>]' value='<?php echo( $_SESSION["qty"][$i] ); ?>' size='2' min="0" max="200" />                        </td>
                        <td ><strong><?php echo( $_SESSION["amounts"][$i]/$_SESSION["qty"][$i] ); ?></strong></td>
                        <td><strong><?php echo( $_SESSION["amounts"][$i] ); ?></strong></td>
                        <td>
                       <a href="?delete=<?php echo($i); ?>">Delete</a></td>
                    </tr>
 <?php
 $total = $total + $_SESSION["amounts"][$i];
 // รวมจำนวนเงินใน เซคชั่น
 }
 $_SESSION["total"] = $total;
 ?>
                    <tr>
                        <td>   </td>
                        <td>   </td>             <td><h5>รวม</h5></td>   <td class="text-right"><h5><strong><?php echo($total); ?> บาท</strong></h5></td>
                        <td>   </td>
                    </tr>
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   <input type="submit" name="button" id="button" value="ปรับปรุง" /></td>

                        <td  colspan="2">
                       <a href="checkout.php"  class="btn btn-success">ยืนยันการสั่งซื้อ</a>
                        </button></td>
                    </tr>
                </tbody>
            </table><?php
 } else { echo "ไม่มีรายการสั่ง";}
 ?></form>

    </div>
  </div>
</div>

                  <?php require_once('down.php'); ?>

</body>
</html>
