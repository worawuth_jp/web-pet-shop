<!--ตารางสรุปรายได้รายเดือน-->

<?php
 $sql_income = 'SELECT `detail_bill`.`id_detail`,`detail_bill`.`id_bill`,`detail_bill`.`id_product`,SUM(`detail_bill`.`qty_product`) as num,SUM(`detail_bill`.`amounts`) as \'Amounts\',Date_format(`bill_product`.`date_bill`,\'%m/%Y\') as \'DATE/MONTH\' FROM detail_bill
INNER JOIN `bill_product` ON `bill_product`.`id_bill` = `detail_bill`.`id_bill`
GROUP BY MONTH(`bill_product`.`date_bill`)';
 $result_income = mysqli_query($con,$sql_income);
?>

<div class="header">
    <h2>รายงายสถิตการขายรายเดือน</h2>
</div>
<table id="income" class="table text-center border-top">
    <thead>
    <tr>
        <th scope="col" class="text-center "><label class="font-weight-bold">เดือน/ปี</label></th>
        <th scope="col" class="text-center"><label class="font-weight-bold">จำนวนสินค้าที่ขาย/เดือน</label></th>
        <th scope="col" class="text-right"><label class="font-weight-bold">ยอดขายรายเดือน</label></th>
    </tr>
    </thead>

    <tbody>
    <tr>
        <?php
            $total = 0;
            while ($record = $result_income->fetch_array()){
                $total += $record['Amounts'];

        ?>
        <td ><?php echo $record['DATE/MONTH'];?></td>
        <td ><?php echo $record['num'];?></td>
        <td class="text-right"><label><?php echo number_format($record['Amounts'], 2); ?></label></td>
    </tr>
        <?php
        }
        ?>
    <tr class="border-bottom">
        <td colspan="2" class="text-right border-right font-weight-bold">รวม</td>
        <td class="text-right font-weight-bold"><?php echo number_format($total,2); ?></td>
    </tr>
    </tbody>
</table>
