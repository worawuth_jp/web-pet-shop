-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2020 at 05:57 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `petshop_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `bill_product`
--

CREATE TABLE `bill_product` (
  `id_bill` int(11) UNSIGNED NOT NULL,
  `total_bill` int(30) NOT NULL,
  `id_member` int(30) NOT NULL,
  `date_bill` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bill_product`
--

INSERT INTO `bill_product` (`id_bill`, `total_bill`, `id_member`, `date_bill`) VALUES
(20, 38158, 11357, '2020-05-14 22:32:36'),
(21, 13010, 11359, '2020-05-14 22:34:00'),
(22, 2730, 11355, '2020-05-14 22:38:59'),
(23, 4946, 11356, '2020-06-14 22:42:54'),
(24, 1996, 11356, '2020-07-14 22:44:08'),
(25, 500, 11355, '2020-07-14 22:55:45'),
(26, 1996, 11358, '2020-08-14 22:56:13');

-- --------------------------------------------------------

--
-- Table structure for table `detail_bill`
--

CREATE TABLE `detail_bill` (
  `id_detail` int(11) UNSIGNED NOT NULL,
  `id_bill` int(10) NOT NULL,
  `id_product` int(10) NOT NULL,
  `qty_product` int(10) NOT NULL,
  `amounts` int(100) NOT NULL,
  `date_detail` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `detail_bill`
--

INSERT INTO `detail_bill` (`id_detail`, `id_bill`, `id_product`, `qty_product`, `amounts`, `date_detail`) VALUES
(52, 26, 11355, 1, 1996, '2020-06-14 22:56:13'),
(51, 25, 11373, 1, 500, '2020-06-14 22:55:45'),
(50, 24, 11355, 1, 1996, '2020-06-14 22:44:08'),
(49, 23, 11373, 1, 500, '2020-06-14 22:42:54'),
(48, 23, 11359, 19, 4446, '2020-06-14 22:42:54'),
(47, 22, 11355, 1, 1996, '2020-06-14 22:38:59'),
(46, 22, 11373, 1, 500, '2020-06-14 22:38:59'),
(45, 22, 11358, 1, 234, '2020-06-14 22:38:59'),
(44, 21, 11359, 15, 3510, '2020-06-14 22:34:00'),
(43, 21, 11373, 19, 9500, '2020-06-14 22:34:00'),
(42, 20, 11358, 1, 234, '2020-06-14 22:32:36'),
(41, 20, 11355, 19, 37924, '2020-06-14 22:32:36');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id_member` int(11) UNSIGNED NOT NULL,
  `type_member` int(3) NOT NULL,
  `name_member` varchar(255) NOT NULL,
  `Username_member` varchar(255) NOT NULL,
  `Password_member` int(50) NOT NULL,
  `date_member` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `email_member` varchar(255) NOT NULL,
  `phone_member` varchar(50) NOT NULL,
  `img_member` varchar(255) NOT NULL,
  `namepet` varchar(255) NOT NULL,
  `sex` varchar(50) NOT NULL,
  `type_pet` varchar(50) NOT NULL,
  `species` varchar(50) NOT NULL,
  `weight` int(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id_member`, `type_member`, `name_member`, `Username_member`, `Password_member`, `date_member`, `email_member`, `phone_member`, `img_member`, `namepet`, `sex`, `type_pet`, `species`, `weight`) VALUES
(1, 1, 'กรณ์ดนัย', 'admin', 1, '2018-11-10 08:19:44', 'admin@hotmail.com', '35453', '', '', '', '', '', 0),
(11355, 0, 'korndanai', 'test', 1, '2020-06-14 20:10:10', 'kkkk@gmail.com', '0955037579', 'ดาวน์โหลด (1).jpg', 'ชีโร่', 'เพศผู้', 'สุนัข', 'กก', 44),
(11356, 0, 'joe', 'joe', 1, '2020-06-14 22:41:28', 'freshy@hotmail.com', '11111', '', 'แดง', 'เพศผู้', 'สุนัข', 'test', 50),
(11357, 0, 'กรณ์ดนัย', 'korn', 1, '2020-06-14 22:10:13', 'freshy@hotmail.com', '11111', '', 'แดง', 'เพศผู้', 'สุนัข', 'test', 50),
(11358, 0, 'เต้', 'teh', 1, '2020-06-14 22:12:53', 'teay@gmail.com', '11111', 'Capture33.JPG', 'ยูช่า', 'เพศผู้', 'สุนัข', 'ปอมๆ', 30),
(11359, 0, 'อ้น', 'aon', 1, '2020-06-14 22:36:55', 'aon_z@gmail.com', '959858m', '', 'บลู', 'เพศผู้', 'สุนัข', 'กก', 33);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id_product` int(11) UNSIGNED NOT NULL,
  `name_product` varchar(100) NOT NULL,
  `type_product` varchar(20) NOT NULL,
  `price_product` int(10) NOT NULL,
  `detail_product` text NOT NULL,
  `img_product` varchar(150) NOT NULL,
  `date_product` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id_product`, `name_product`, `type_product`, `price_product`, `detail_product`, `img_product`, `date_product`) VALUES
(11353, 'โปรแกรมตรวจสุขภาพสัตว์เลี้ยง', '2', 1300, 'คุ้มสุด โปรแกรมตรวจสุขภาพสัตว์เลี้ยง สำหรับน้องสุนัข อายุ 5 ปีขึ้นไป+โค๊ดส่วนลดพิเศษ\r\nสามารถตรวจสอบรายการตรวจสุขภาพทั้งหมดที่จะได้รับการตรวจ ที่คลังรูปภาพ\r\n\r\nหากมีข้อสงสัยเกี่ยวกับโปรแกรม ติดต่อเจ้าหน้าที่ได้โดยตรงทาง Line Official : @thonglorpetshop หรือโทร 020799999 ได้ตลอด 24 ชั่วโมง เจ้าหน้าที่จะทำการติดต่อกลับทันที\r\n\r\nหมายเหตุ\r\n1 แพ็กเกจนี้สำหรับการตรวจเช็คสุขภาพแผนกผู้ป่วยนอกเท่านั้น\r\n2 ราคาดังกล่าวรวมค่าบริการทางการแพทย์ ค่าบริการพยาบาล ค่าบริการโรงพยาบาล และ VAT แล้ว\r\n3 ในกรณีที่ไม่สามารถเก็บปัสสาวะในวันที่ตรวจสุขภาพได้ สามารถเก็บปัสสาวะมาตรวจภายหลังได้ ภายใน 14 วันหลังตรวจสุขภาพ\r\n4 ขอสงวนสิทธิ์ในการเปลี่ยนแปลง แก้ไข และยกเลิกโดยไม่ต้องแจ้งให้ทราบล่วงหน้า', '222-1.jpg', '2020-06-14 22:51:54'),
(11355, 'โปรแกรมตรวจสุขภาพสัตว์เลี้ยง  สำหรับน้องสุนัข อายุไม่เกิน 5 ปี', '1', 1996, 'หากมีข้อสงสัยเกี่ยวกับโปรแกรม ติดต่อเจ้าหน้าที่ได้โดยตรงทาง Line Official : @thonglorpetshop หรือโทร 020799999 ได้ตลอด 24 ชั่วโมง เจ้าหน้าที่จะทำการติดต่อกลับทันที\r\n\r\nหมายเหตุ\r\n1 แพ็กเกจนี้สำหรับการตรวจเช็คสุขภาพแผนกผู้ป่วยนอกเท่านั้น\r\n2 ราคาดังกล่าวรวมค่าบริการทางการแพทย์ ค่าบริการพยาบาล ค่าบริการโรงพยาบาล และ VAT แล้ว\r\n3 ในกรณีที่ไม่สามารถเก็บปัสสาวะในวันที่ตรวจสุขภาพได้ สามารถเก็บปัสสาวะมาตรวจภายหลังได้ ภายใน 14 วันหลังตรวจสุขภาพ\r\n4 ขอสงวนสิทธิ์ในการเปลี่ยนแปลง แก้ไข และยกเลิกโดยไม่ต้องแจ้งให้ทราบล่วงหน้า', '111-1536x1536.jpg', '2020-06-14 22:51:20'),
(11358, '(E-Voucher) อาบน้ำสุนัขขน 2 ชั้น โรงพยาบาลสัตว์ทองหล่อ', '6', 3000, 'ราคารวม อาบน้ำสุนัข+ตัดเล็บ+เช็ดหู+แปรงขน (ราคาไม่รวมตัดขน)\r\nใช้ได้กับโรงพยาบาลสัตว์ทองหล่อทุกสาขา\r\nคูปองใช้ภายใน 60 วันนับจากวันที่ซื้อ\r\nเราอาบน้ำด้วยน้ำอุ่นทุกครั้ง\r\nการเป่าขนของเรามีขั้นตอนที่ต่างจากที่อื่นคือ หลังจากอาบน้ำเราจะเริ่มจากการไล่น้ำด้วยไดร์อุ่น และพักด้วยลมเย็นเพื่อเก็บความชุ่มชื้นให้ผิว ป้องกันผิวแห้ง และจบด้วยไดร์อุ่นอีกครั้งในการแต่งทรงให้สวยงาม\r\nช่างของเราทุกคนได้รับการฝึกฝนจนมีความชำนาญ ไม่ใช่แค่การอาบน้ำตัดขน แต่ยังถูกฝึกมาให้สังเกตุน้องๆในทุกจุดและทุกขั้นตอนระหว่างการอาบน้ำตัดขน เพื่อที่จะสามารถบอกความผิดปกติที่มีหรือเกิดขึ้นกับน้องๆได้', 'Dog-1-5-KG.-5.01-10-KG.-10.01-15-KG.-15.01-20-KG.-20.01-30-KG.-30.01-40-KG.-40.01-KG.UP-ขนสั้น-300-380-480-580-680-780-880-2.png', '2020-06-14 22:53:26'),
(11359, '(แมว) โปรแกรมดูแลสุขภาพสัตว์เลี้ยง Thonglor Smile Wellness Plan', '1', 4000, 'Thonglor Smile Wellness Plan คือโปรแกรมดูแลสุขภาพของสัตว์เลี้ยงแสนรัก ซึ่งจะดูแลทั้งปีในราคาน่ารัก โดยแบ่งเป็น โปรแกรมลูกแมว(อายุน้อยกว่า 6 เดือน) และ แมวโต สามารถใช้ได้กับโรงพยาบาลสัตว์ทองหล่อทุกสาขา ซึ่งครอบคลุมการดูแลดังนี้\r\n\r\nวัคซีนประจำปีลูกแมวและแมว (วัคซีนไข้หัดแมวและหวัดแมว) วัคซีนพิษสุนัขบ้า\r\nตรวจเลือด ถ่ายพยาธิ กำจัดเห็บหมัด ไรขี้เรื้อน ไรในหู ป้องกันพยาธิหนอนหัวใจ พบสัตวแพทย์เพื่อตรวจสุขภาพเบื้องต้น\r\nบีบต่อม เช็ดหู ตัดเล็บ โดยสัตวแพทย์ ปรึกษาปัญหาสุขภาพทั่วไป 24 ชั่วโมง\r\nและฟรีบริการอาบน้ำพร้อมโอโซนสปา\r\nโปรแกรมดูแลสุขภาพสำหรับลูกแมว(อายุน้อยกว่า 6 เดือน)และแมวโต เฉลี่ยเดือนละ 833 บาท- (9,999 บาทต่อปี)', 'cad057f35315d76c82e9c4533742e948b_28264573_200211_0012-1.jpg', '2020-06-14 22:54:22'),
(11373, '(E-Voucher) Pet pool สระว่ายน้ำสุนัขขน 2 ชั้น โดยโรงพยาบาลสัตว์ทองหล่อ', '11', 500, 'ราคารวม: ว่ายน้ำ+อาบน้ำ+เป่าขน+ตัดเล็บ+เช็ดหู\r\nคูปองใช้ภายใน 60 วันนับจากวันที่ซื้อ\r\nมั่นใจในคุณภาพสระว่ายน้ำของโรงพยาบาลสัตว์ทองหล่อ\r\nสระว่ายน้ำระบบน้ำเกลือ ปลอดสารเคมี ไม่เป็นอันตรายต่อผิวหนังสุนัข\r\nสระระบบน้ำล้น ดันขนออกจากสระน้ำ ทำให้มีความสะอาดอยู่เสมอ\r\nมีพี่เลี้ยงดูแลปลอดภัยแม้พึ่งเคยมาครั้งแรก\r\nสามารถใช้ได้กับโรงพยาบาลสัตว์ทองหล่อเฉพาะสาขาที่มีสระว่ายน้ำเท่านั้น', 'Dog-1-5-KG.-5.01-10-KG.-10.01-15-KG.-15.01-20-KG.-20.01-30-KG.-30.01-40-KG.-40.01-KG.UP-ขนสั้น-300-380-480-580-680-780-880-5-1.png', '2020-06-14 22:55:19');

-- --------------------------------------------------------

--
-- Table structure for table `type_product`
--

CREATE TABLE `type_product` (
  `id_type` int(11) UNSIGNED NOT NULL,
  `name_type` varchar(255) NOT NULL,
  `date_member` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type_product`
--

INSERT INTO `type_product` (`id_type`, `name_type`, `date_member`) VALUES
(1, 'บริการประเภทสุนัขขนยาว', '2020-06-14 20:22:28'),
(2, 'ประเภทสุนัขขนสั้น', '2020-06-14 20:22:59'),
(6, 'ประเภทแมวขนยาว', '2020-06-14 20:23:10'),
(11, 'ประเภทแมวขนสั้น', '2020-06-14 20:23:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bill_product`
--
ALTER TABLE `bill_product`
  ADD PRIMARY KEY (`id_bill`);

--
-- Indexes for table `detail_bill`
--
ALTER TABLE `detail_bill`
  ADD PRIMARY KEY (`id_detail`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id_member`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id_product`);

--
-- Indexes for table `type_product`
--
ALTER TABLE `type_product`
  ADD PRIMARY KEY (`id_type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bill_product`
--
ALTER TABLE `bill_product`
  MODIFY `id_bill` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `detail_bill`
--
ALTER TABLE `detail_bill`
  MODIFY `id_detail` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id_member` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11360;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id_product` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11374;

--
-- AUTO_INCREMENT for table `type_product`
--
ALTER TABLE `type_product`
  MODIFY `id_type` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
