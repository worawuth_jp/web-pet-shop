
<?php
include 'function.inc.php';

include 'chk_sess.php';

$query = sprintf('select * from member where id_member="%s" ',$_SESSION['mid']);

$result = mysqli_query($con,$query);
$rs = mysqli_fetch_array($result);

$Username_member=$rs['Username_member'];
$Password_member=$rs['Password_member'];
$name_member=$rs['name_member'];
$lastname_member=$rs['lastname_member'];
$email_member=$rs['email_member'];

$address_member=$rs['address_member'];

$phone_member=$rs['phone_member'];

?>

<!DOCTYPE html>

<html lang="en">
<head>
<title><?php echo $sys_title ?></title><meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="e-commerce site well design with responsive view." />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link href="../seminar_test/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<link href="../seminar_test/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="../seminar_test/css/stylesheet.css" rel="stylesheet">
<link href="../seminar_test/css/responsive.css" rel="stylesheet">
<link href="../seminar_test/owl-carousel/owl.carousel.css" type="text/css" rel="stylesheet" media="screen" />
<link href="../seminar_test/owl-carousel/owl.transitions.css" type="text/css" rel="stylesheet" media="screen" />
<script src="../seminar_test/javascript/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="../seminar_test/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../seminar_test/javascript/jstree.min.js"></script>
<script type="text/javascript" src="../seminar_test/javascript/template.js"></script>
<script src="../seminar_test/javascript/common.js" type="text/javascript"></script>
<script src="../seminar_test/javascript/global.js" type="text/javascript"></script>
<script src="../seminar_test/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
</head>
<body class="account-register col-2">


<?php
include 'header.php'
?>
   
<div class="container">
    <ul class="breadcrumb">
        <li><a href="../seminar_test/index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="#">Account</a></li>
        <li><a href="../seminar_test/profile.php">ประเมินความพึ่งพอใจ</a></li>
    </ul>
    <div class="row">
        <div class="col-sm-3 hidden-xs column-left" id="column-center">
            <div class="column-block">
                
            </div>
        </div>
        <div class="col-sm-9" id="content">
            <h1>ประเมินความพึ่งพอใจ</h1>
<form id="formqsys" name="formqsys" method="post" action="../seminar_test/save_db.php">
    <table width="70%" border="1" align="center" cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
            <tr>
              <td width="75%" rowspan="2" align="center">
              <br>
              <strong>หัวข้อการประเมิน</strong>
              </td>
              <td colspan="5" align="center"><strong>ระดับความพึงพอใจ</strong></td>
            </tr>
            <tr>
              <td width="5%" align="center"><strong>5</strong></td>
              <td width="5%" align="center"><strong>4</strong></td>
              <td width="5%" align="center"><strong>3</strong></td>
              <td width="5%" align="center"><strong>2</strong></td>
              <td width="5%" align="center"><strong>1</strong></td>
            </tr>
            <tr>
              <td height="30">&nbsp; 1.การออกแบบตัวละคร</td>
              <td height="30" align="center"><input type="radio" name="es_id1<?php $ic;?>"  value="5" required /></td>
              <td height="30" align="center"><input type="radio" name="es_id1<?php $ic;?>"  value="4" /></td>
              <td height="30" align="center"><input type="radio" name="es_id1<?php $ic;?>"  value="3" /></td>
              <td height="30" align="center"><input type="radio" name="es_id1<?php $ic;?>"  value="2" /></td>
              <td height="30" align="center"><input type="radio" name="es_id1<?php $ic;?>"  value="1" /></td>
            </tr>
            <tr>
              <td height="30">&nbsp; 2.ความสวยงามของฉาก</td>
              <td width="5%" height="30" align="center"><input type="radio" name="es_id2<?php $ic;?>"  value="5" required /></td>
              <td width="5%" height="30" align="center"><input type="radio" name="es_id2<?php $ic;?>"  value="4"/></td>
              <td width="5%" height="30" align="center"><input type="radio" name="es_id2<?php $ic;?>"  value="3"/></td>
              <td width="5%" height="30" align="center"><input type="radio" name="es_id2<?php $ic;?>"  value="2"/></td>
              <td width="5%" height="30" align="center"><input type="radio" name="es_id2<?php $ic;?>"  value="1"/></td>
            </tr>
            <tr>
              <td height="30">&nbsp; 3.การดำเนินของเนื้อเรื่อง</td>
              <td width="5%" height="30" align="center"><input type="radio" name="es_id3<?php $ic;?>"  value="5" required /></td>
              <td width="5%" height="30" align="center"><input type="radio" name="es_id3<?php $ic;?>"  value="4"/></td>
              <td width="5%" height="30" align="center"><input type="radio" name="es_id3<?php $ic;?>"  value="3"/></td>
              <td width="5%" height="30" align="center"><input type="radio" name="es_id3<?php $ic;?>"  value="2"/></td>
              <td width="5%" height="30" align="center"><input type="radio" name="es_id3<?php $ic;?>"  value="1"/></td>
            </tr>
            <tr>
              
              <td height="30">&nbsp; 4.ความคมชัดของระบบเสียง</td>
              <td width="5%" height="30" align="center"><input type="radio" name="es_id4<?php $ic;?>"  value="5" required /></td>
              <td width="5%" height="30" align="center"><input type="radio" name="es_id4<?php $ic;?>"  value="4"/></td>
              <td width="5%" height="30" align="center"><input type="radio" name="es_id4<?php $ic;?>"  value="3"/></td>
              <td width="5%" height="30" align="center"><input type="radio" name="es_id4<?php $ic;?>"  value="2"/></td>
              <td width="5%" height="30" align="center"><input type="radio" name="es_id4<?php $ic;?>"  value="1"/></td>
            </tr>
            <tr>
           <td height="30">&nbsp; 5.ความสนุกสนานที่ได้รับชม</td>
              <td height="30" align="center"><input type="radio" name="es_id5"  value="5" required /></td>
              <td height="30" align="center"><input type="radio" name="es_id5"  value="4"/></td>
              <td height="30" align="center"><input type="radio" name="es_id5"  value="3"/></td>
              <td height="30" align="center"><input type="radio" name="es_id5"  value="2"/></td>
              <td height="30" align="center"><input type="radio" name="es_id5"  value="1"/></td>
            </tr>
            <tr>
              <td height="30">&nbsp; 6.สาระน่ารู้ที่สอดแทรกในเนื้อเรื่อง</td>
              <td height="30" align="center"><input type="radio" name="es_id6"  value="5" required /></td>
              <td height="30" align="center"><input type="radio" name="es_id6"  value="4"/></td>
              <td height="30" align="center"><input type="radio" name="es_id6"  value="3"/></td>
              <td height="30" align="center"><input type="radio" name="es_id6"  value="2"/></td>
              <td height="30" align="center"><input type="radio" name="es_id6"  value="1"/></td>
            </tr>
            <tr>
              <td height="30">&nbsp; 7.ประโยชน์ที่ได้รับจากการชม</td>
              <td height="30" align="center"><input type="radio" name="es_id7" value="5" required /></td>
              <td height="30" align="center"><input type="radio" name="es_id7"  value="4"/></td>
              <td height="30" align="center"><input type="radio" name="es_id7"  value="3"/></td>
              <td height="30" align="center"><input type="radio" name="es_id7"  value="2"/></td>
              <td height="30" align="center"><input type="radio" name="es_id7"  value="1"/></td>
            </tr>
            <tr>
              <td height="30">&nbsp; 8.การใช้ถ้อยคำที่เหมาะสม</td>
              <td height="30" align="center"><input type="radio" name="es_id8"  value="5" required /></td>
              <td height="30" align="center"><input type="radio" name="es_id8"  value="4"/></td>
              <td height="30" align="center"><input type="radio" name="es_id8"  value="3"/></td>
              <td height="30" align="center"><input type="radio" name="es_id8"  value="2"/></td>
              <td height="30" align="center"><input type="radio" name="es_id8"  value="1"/></td>
            </tr>
            <tr>
              <td height="30">&nbsp; 9.เนื้อเรื่องเข้าใจง่าย</td>
              <td height="30" align="center"><input type="radio" name="es_id9"  value="5" required /></td>
              <td height="30" align="center"><input type="radio" name="es_id9" value="4"/></td>
              <td height="30" align="center"><input type="radio" name="es_id9" value="3"/></td>
              <td height="30" align="center"><input type="radio" name="es_id9" value="2"/></td>
              <td height="30" align="center"><input type="radio" name="es_id9" value="1"/></td>
            </tr>
            <tr>
              <td height="30">&nbsp; 10.การลำดับเหตุการณ์ของเรื่อง</td>
              <td height="30" align="center"><input type="radio" name="es_id10"  value="5" required /></td>
              <td height="30" align="center"><input type="radio" name="es_id10"  value="4"/></td>
              <td height="30" align="center"><input type="radio" name="es_id10"  value="3"/></td>
              <td height="30" align="center"><input type="radio" name="es_id10"  value="2"/></td>
              <td height="30" align="center"><input type="radio" name="es_id10"  value="1"/></td>
            </tr>
           <tr>
              <td height="30">&nbsp; 11.ความถูกต้องของข้อมูล</td>
              <td height="30" align="center"><input type="radio" name="es_id11"   value="5" required /></td>
              <td height="30" align="center"><input type="radio" name="es_id11"   value="4"/></td>
              <td height="30" align="center"><input type="radio" name="es_id11"   value="3"/></td>
              <td height="30" align="center"><input type="radio" name="es_id11"   value="2"/></td>
              <td height="30" align="center"><input type="radio" name="es_id11"   value="1"/></td>
            </tr>
            <!-- <tr>
              <td height="30">&nbsp; 12.ความถูกต้องในการเชื่อมโยงภายในระบบ</td>
              <td height="30" align="center"><input type="radio" name="es_id12"  value="5" required /></td>
              <td height="30" align="center"><input type="radio" name="es_id12"  value="4"/></td>
              <td height="30" align="center"><input type="radio" name="es_id12"  value="3"/></td>
              <td height="30" align="center"><input type="radio" name="es_id12"  value="2"/></td>
              <td height="30" align="center"><input type="radio" name="es_id12"  value="1"/></td>
            </tr>
            <tr>
              <td height="30">&nbsp; 13.ความปลอดภัยในการใช้งาน</td>
              <td height="30" align="center"><input type="radio" name="es_id13"  value="5" required /></td>
              <td height="30" align="center"><input type="radio" name="es_id13"  value="4"/></td>
              <td height="30" align="center"><input type="radio" name="es_id13"  value="3"/></td>
              <td height="30" align="center"><input type="radio" name="es_id13"  value="2"/></td>
              <td height="30" align="center"><input type="radio" name="es_id13"  value="1"/></td>
            </tr>
            <tr>
              <td height="30">&nbsp; 14.ความสะดวกสบายในการใช้งานได้ทุกที่ทุกเวลา โดยไม่ต้องเข้ามาใช้งานภายมหาวิทยาลัย</td>
              <td height="30" align="center"><input type="radio" name="es_id14"  value="5" required /></td>
              <td height="30" align="center"><input type="radio" name="es_id14"  value="4"/></td>
              <td height="30" align="center"><input type="radio" name="es_id14"  value="3"/></td>
              <td height="30" align="center"><input type="radio" name="es_id14"  value="2"/></td>
              <td height="30" align="center"><input type="radio" name="es_id14"  value="1"/></td>
            </tr>
            <tr>
              <td height="30">&nbsp; 15.สามารถดูข้อมูลย้อนหลังหรือประวัติการฝึกประสบการณ์ได้</td>
              <td height="30" align="center"><input type="radio" name="es_id15"  value="5" required /></td>
              <td height="30" align="center"><input type="radio" name="es_id15"  value="4"/></td>
              <td height="30" align="center"><input type="radio" name="es_id15"  value="3"/></td>
              <td height="30" align="center"><input type="radio" name="es_id15"  value="2"/></td>
              <td height="30" align="center"><input type="radio" name="es_id15"  value="1"/></td>
            </tr>
            <tr>
              <td height="30">&nbsp; 16.สามารถพิมพ์(print)เอกสารได้โดยตรงจากระบบ</td>
              <td height="30" align="center"><input type="radio" name="es_id16"  value="5" required /></td>
              <td height="30" align="center"><input type="radio" name="es_id16"  value="4"/></td>
              <td height="30" align="center"><input type="radio" name="es_id16"  value="3"/></td>
              <td height="30" align="center"><input type="radio" name="es_id16"  value="2"/></td>
              <td height="30" align="center"><input type="radio" name="es_id16"  value="1"/></td>
            </tr>
            <tr>
              <td height="30">&nbsp; 17.ความพึงพอใจต่อระบบโดยรวม</td>
              <td height="30" align="center"><input type="radio" name="es_id17"  value="5" required /></td>
              <td height="30" align="center"><input type="radio" name="es_id17"  value="4"/></td>
              <td height="30" align="center"><input type="radio" name="es_id17"  value="3"/></td>
              <td height="30" align="center"><input type="radio" name="es_id17"  value="2"/></td>
              <td height="30" align="center"><input type="radio" name="es_id17"  value="1"/></td>
            </tr>-->
          </table>
          </div>
          </div>
          <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6">
          <br /><br />
          <b> ข้อเสนอแนะเพิ่มเติม </b> <br />
           <textarea name="es_complain" cols="80" rows="3" id="es_complain" class="form-control"></textarea>
                <br /><input name="id" type="hidden" id="id" value="<?php echo $rs['id_member']?>" />

              <button type="submit" name="save" class="btn btn-primary"> ส่งแบบประเมิน </button>
        
          </div>
          </div>
          
        </form>
        </div>
    </div>


<?php
include 'footer.php'
?>
</body>
</html>
